require 'rails_helper'

RSpec.describe NavCell, type: :cell do
  context 'cell rendering' do
    context 'show' do
      it 'has the expected content' do
        content = cell(:nav).call
        puts 'Cell content : ' + content.inspect
        expect(content).to have_link('Home', href: Rails.application.routes.url_helpers.root_path)
      end
    end
  end
end #
