require 'test_helper'

class NavCellTest < Cell::TestCase
  test 'show' do
    html = cell('nav').call(:show)
    assert html.match /<a href="\/">/
  end


end
