This project contains a simple cell and tests for it.

The cell is using Rails application route: 

``` ruby
class NavCell < Cell::ViewModel
  include Cell::Slim
  def show
    link_to("Link to home", root_path)
  end
end
```

This works fine in a rails server session.
On running the tests we see the problem:

``` ruby
require 'rails_helper'

RSpec.describe NavCell, type: :cell do
  context 'cell rendering' do
    context 'show' do
      it 'has the expected content' do
        content = cell(:nav).call
        puts 'Cell content : ' + content.inspect
        expect(content).to have_link('Home', href: Rails.application.routes.url_helpers.root_path)
      end
    end
  end
end 
```


```
$ rspec
F

Failures:

  1) NavCell cell rendering show has the expected content
     Failure/Error: link_to("Link to home", root_path)
     
     NoMethodError:
       undefined method `url_options' for nil:NilClass
     # ./app/cells/nav_cell.rb:4:in `show'
     # /home/CJBrew/.rvm/gems/ruby-2.3.0/gems/cells-4.1.3/lib/cell/view_model.rb:100:in `render_state'
     # /home/CJBrew/.rvm/gems/ruby-2.3.0/gems/cells-4.1.3/lib/cell/caching.rb:47:in `render_state'
     # /home/CJBrew/.rvm/gems/ruby-2.3.0/gems/cells-4.1.3/lib/cell/view_model.rb:78:in `call'
     # /home/CJBrew/.rvm/gems/ruby-2.3.0/gems/cells-rails-0.0.6/lib/cell/rails.rb:54:in `call'
     # ./spec/cells/nav_cell_spec.rb:7:in `block (4 levels) in <top (required)>'

Finished in 0.00247 seconds (files took 2.39 seconds to load)
1 example, 1 failure

Failed examples:

rspec ./spec/cells/nav_cell_spec.rb:6 # NavCell cell rendering show has the expected content
```

A similar test fails in standard rails tests too:

```
$ rails test
Running via Spring preloader in process 25641
Run options: --seed 13521

# Running:

.E

Error:
NavCellTest#test_show:
NoMethodError: undefined method `url_options' for nil:NilClass
    app/cells/nav_cell.rb:4:in `show'
    test/cells/nav_cell_test.rb:5:in `block in <class:NavCellTest>'


bin/rails test test/cells/nav_cell_test.rb:4
```